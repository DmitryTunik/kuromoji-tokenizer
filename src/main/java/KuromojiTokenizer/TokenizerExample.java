package KuromojiTokenizer;

import java.util.List;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.StringJoiner;

import org.json.*;

import KuromojiTokenizer.TokanizationHandler;
import com.atilika.kuromoji.ipadic.Token;

public class TokenizerExample {
  public static void main(String[] args) {
    TokanizationHandler tokanizationHandler = new TokanizationHandler();
    String fileName = args[0];
    JSONObject result = new JSONObject();
    try {
      Scanner scanner = new Scanner(new File(fileName));
      int i = 0;
      while (scanner.hasNextLine()) {
        String sample = scanner.nextLine();
        StringJoiner sb = new StringJoiner(", ");
        List<Token> tokens = tokanizationHandler.tokenize(sample);
        for (Token token : tokens) {
          String tokenSurface = token.getSurface();
          if (!tokenSurface.trim().isEmpty()) {
            sb.add(token.getSurface());
          }
          // result.put(token.getSurface(), token.getAllFeatures());
        }
        JSONObject sampleResult = new JSONObject();

        sampleResult.put("sample_" + i, sample);
        sampleResult.put("result_" + i, sb.toString());
        result.put(Integer.toString(i), sampleResult);
        i++;
      }
      scanner.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    String printableResult = result.toString(4);
    PrintWriter out = null;
    try {
      out = new PrintWriter("result.txt");
      out.println(printableResult);
      out.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

  }
}