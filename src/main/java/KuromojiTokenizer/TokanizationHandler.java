package KuromojiTokenizer;

import java.util.List;
import java.lang.String;

import com.atilika.kuromoji.TokenizerBase.Builder;
import com.atilika.kuromoji.TokenizerBase.Mode;
import com.atilika.kuromoji.ipadic.Token;
import com.atilika.kuromoji.ipadic.Tokenizer;

public class TokanizationHandler {
  Tokenizer tokenizer;

  public TokanizationHandler() {
    Builder builder = new Tokenizer.Builder().mode(Mode.SEARCH);
    tokenizer = builder.build();
  }

  public List<Token> tokenize(String inputToBeTokenized) {
    List<Token> tokens = tokenizer.tokenize(inputToBeTokenized);
    return tokens;
  }
}